Source: thonny
Maintainer: Debian Edu Packaging Team <debian-edu-pkg-team@lists.alioth.debian.org>
Uploaders:
 Aivar Annamaa <aivar.annamaa@gmail.com>,
 Dominik George <natureshadow@debian.org>,
Section: devel
Priority: optional
Build-Depends:
 debhelper (>= 11),
 dh-exec,
 dh-python,
 mypy,
 pylint,
 python3-all,
 python3-docutils,
 python3-jedi (>= 0.10.1~),
 python3-send2trash,
 python3-serial,
 python3-setuptools,
 python3-tk,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-edu-pkg-team/thonny
Vcs-Git: https://salsa.debian.org/debian-edu-pkg-team/thonny.git
Homepage: https://thonny.org
Testsuite: autopkgtest-pkg-python

Package: thonny
Architecture: all
Depends:
 mypy,
 pylint,
 python3-asttokens,
 python3-pip,
 python3-pkg-resources,
 python3-tk,
 python3-venv,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 xsel,
 zenity,
 ${python3:Recommends},
Suggests:
 python3-distro,
 ${python3:Suggests},
Description: Python IDE for beginners
 Thonny is a simple Python IDE with features useful for learning programming.
 .
 It comes with a debugger which is able to visualize all the conceptual steps
 taken to run a Python program (executing statements, evaluating expressions,
 maintaining the call stack). There is a GUI for installing 3rd party packages
 and special mode for learning about references.
 .
 See the homepage for more information and screenshots.
