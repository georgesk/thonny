thonny (3.2.7-2.0) unstable; urgency=medium

  * non-maintainer upload
  * fixed a po file's exec flag, uodated the French tranlation
  * checked that python3-venv "went back" (Closes: #970808)

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 26 Oct 2020 14:11:15 +0100

thonny (3.2.7-1) unstable; urgency=medium

  * New upstream version (Closes: #950884).
  * Bump Standards-Version (no changes needed).

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Mon, 10 Feb 2020 22:12:16 +0200

thonny (3.2.1-1) unstable; urgency=medium

  * New upstream version.
  * Change pylint3 dependency to pylint. (Closes: #944831)

 -- Dominik George <natureshadow@debian.org>  Thu, 21 Nov 2019 11:14:41 +0100

thonny (3.2.0-1) unstable; urgency=medium

  * New upstream version.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Mon, 12 Aug 2019 20:12:16 +0200

thonny (3.1.2-1) unstable; urgency=medium

  * New upstream version.

 -- Dominik George <natureshadow@debian.org>  Fri, 22 Feb 2019 20:12:16 +0100

thonny (3.1.1-1) unstable; urgency=medium

  * New upstream version.
  * Depend on asttokens now it is in Debian.

 -- Dominik George <natureshadow@debian.org>  Tue, 12 Feb 2019 12:55:27 +0100

thonny (3.1.0-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version.

  [ Dominik George ]
  * Add Recommends and Suggests from Python helper.
  * Remove duplicate explicit dependencies.

 -- Dominik George <natureshadow@debian.org>  Wed, 30 Jan 2019 13:23:59 +0100

thonny (3.0.8-1) unstable; urgency=medium

  * New upstream version.

 -- Dominik George <natureshadow@debian.org>  Sat, 01 Dec 2018 12:23:58 +0100

thonny (3.0.5-1) unstable; urgency=medium

  * New upstream version 3.0.5

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Sat, 27 Oct 2018 19:13:21 +0200

thonny (3.0.4-1) unstable; urgency=medium

  [ Aivar Annamaa ]
  * New upstream version 3.0.4
  * Add required dependencies:
    - python3-docutils
    - python3-serial
    - python3-pyperclip
    - pylint3
    - mypy
  * Add recommended dependencies:
    - zenity
    - xsel
  * Add suggested dependency: python3-distro
  * Update Thonny homepage (https instead of http)
  * Point docs to README.rst

  [ Dominik George ]
  * Fix version requirement for jedi.
  * Remove code for using help.rst as README.
  * Rename all binary-package related files to thonny.*.

 -- Dominik George <natureshadow@debian.org>  Fri, 26 Oct 2018 18:48:24 +0200

thonny (2.1.22-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * Depend on pkg-resources. (Closes: #904771)

  [ Aivar Annamaa ]
  * New upstream version 2.1.22
  * Add python3-pkg-resources as dependency (by Mike Gabriel)
  * Update debian/watch to ignore beta versions

  [ Dominik George ]
  * Update my mail address as Uploader.
  * Update dependency to jedi to >= 0.10.
  * Fix installation of resources after Python 3.7 was enabled.
  * Bump Standards-Version to 4.2.1.
    + No changes needed.

 -- Dominik George <natureshadow@debian.org>  Thu, 27 Sep 2018 10:56:42 +0200

thonny (2.1.17-1) unstable; urgency=medium

  [ Dominik George ]

  * Move to Salsa.

  [ Aivar Annamaa ]

  * New upstream version 2.1.17

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Fri, 23 Mar 2018 15:26:21 +0200

thonny (2.1.16-3) unstable; urgency=medium

  * Install icons to correct location.

 -- Aivar Annamaa <aivar.annamaa@gmail.com>  Wed, 14 Feb 2018 20:36:15 +0200

thonny (2.1.16-2) unstable; urgency=medium

  * Install desktop and metainfo files to correct location.
  * Bump Standards-Version and compat level (no changes needed).
  * Rename lintian tag debian-watch-does-not-check-gpg-signature.
  * Use HTTPS URL for copyright format.

 -- Dominik George <nik@naturalnet.de>  Tue, 13 Feb 2018 15:17:43 +0100

thonny (2.1.16-1) unstable; urgency=medium

  * Initial release (Closes: #857042)

 -- Dominik George <nik@naturalnet.de>  Sun, 03 Dec 2017 18:00:55 +0100
